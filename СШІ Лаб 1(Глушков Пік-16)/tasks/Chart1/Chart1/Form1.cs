﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chart1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public float function(float x, float a, float b, float c)
        {
            if (x <= a)
            {
                return 0;
            }
            if (a < x && x <= c)
            {
                return (x - a) / (c - a);
            }
            if (c<x && x<b)
            {
                return (b - x) / (b - c);
            }
            if (x >= b)
            {
                return 0;
            }
            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            chart1.Series["q"].Points.Clear();
            chart1.ChartAreas[0].AxisX.Interval = 1; 

            float a = float.Parse(textA.Text);
            float b = float.Parse(textB.Text);
            float c = float.Parse(textC.Text);
            float start = float.Parse(textStart.Text);
            float end = float.Parse(textEnd.Text);

            float y;
            for(float x=start; x<=end; x += 0.5f)
            {
                y = function(x, a, b, c);
                chart1.Series["q"].Points.AddXY(x, y);
            }
        }
    }
}

